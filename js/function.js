/**
 * Created by walter on 10/26/16.
 */

function dataLoad(filename)  {
    var arraydata;
    $.ajax({
        type: "GET",
        url: filename,
        dataType: "json",
        async: false,
        success: function(json) {arraydata = eval(json) }
    });
    return arraydata;
}
function GetQueryString(name)
{
    var reg = new RegExp("(^|&)"+ name +"=([^&]*)(&|$)");
    var r = window.location.search.substr(1).match(reg);
    if(r!=null)
        return  (r[2]);
    return null;
}
function checkMobile(str) {
    var re = /^1\d{10}$/;
    if (re.test(str)) {

        return true;
    } else {
        alert("您输入的电话号码格式不正确!");
        return false;
    }
}
function prizeLeve(state,id){
    $(".wrap").fadeIn();
    const leveOne = "<div class='inner'>" +
        "<div class='innerHeader'>恭喜宝宝获得限量暖心奖</div>" +
        "<div class='LeveOneTag'></div>" +
        "<div class='innerFooter' style='margin-top: '>恭喜宝妈获得价值288元的:<br>32G冒险岛限量定制蘑菇U盘一个。<br>我们将在7个工作日内安排发货,<br>请注意查收!</div>" +
        "</div>" +
        "<div class='cancelButt'></div>";
    const leveTwo = "<div class='inner'>" +
        "<div class='innerHeader' style='line-height: 108px;'>恭喜宝妈获得限量至尊奖</div>" +
        "<div class='LeveTwoTag'></div>" +
        "<div class='innerFooter' style='text-align: center'>恭喜宝妈获得价值188元的<br>冒险岛限量抱枕一个。<br>我们将在7个工作日内安排发货,<br>请注意查收!</div>" +
        "</div>" +
        "<div class='cancelButt'></div>";
    const leveThree = "<div class='inner'>" +
        "<div class='innerHeader' style='line-height: 108px;'>真遗憾</div>" +
        "<div class='LeveThreeTag'></div>" +
        "<div class='innerFooter'>感谢宝妈对活动的支持!<br>关注栗子妈妈公号,<br>更多福利等着您~</div>" +
        "</div>" +
        "<div class='cancelButt'></div>";
    const leveFour = "<div class='inner' style='height: 585px'>" +
        "<div class='innerHeader'>恭喜宝宝获得幸运礼包奖</div>" +
        "<div class='LeveFourTag'></div>" +
        "<div class='innerFooter' style='text-align: center;height: 128px;margin-top: -5px'>恭喜宝妈获得<br>价值188元冒险岛限量定制礼包</div>" +
        "<div class='getCoupon'>点击获取兑换码</div>" +
        "</div>" +"<div class='cancelButt'></div>";
    if(state == 1){
        $(".wrap").html("")
        $(".wrap").html(leveOne);
    }else if (state == 2){
        $(".wrap").html("")
        $(".wrap").html(leveTwo);
    }else if (state == 3){
        $(".wrap").html("")
        $(".wrap").html(leveFour);
        $(".getCoupon").get(0).addEventListener("touchstart",function (e) {
            e.preventDefault();
            $(this).addClass("pressClass");
        })
        $(".getCoupon").get(0).addEventListener("touchend",function (e) {
            e.preventDefault();
            $(this).removeClass("pressClass");
            window.location.href = "getCoupon.html?item="+id
        })
    }else {
        $(".wrap").html("")
        $(".wrap").html(leveThree);
    }
    $(".cancelButt").get(0).addEventListener("click",function (e) {
        e.preventDefault();
        $(".wrap").fadeOut();
    })
}